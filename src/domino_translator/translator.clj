(ns domino-translator.translator
  (:require 
    [domino-translator.translator.bing_translator :as msft]
    [domino-translator.translator.yandex_translator :as yan])
  (:use domino-translator.translator.protocols))

(defn bing []
  (msft/->Bing-translator "not-a-key"))

(defn yndx []
  (yan/->Yandex-translator #{}))