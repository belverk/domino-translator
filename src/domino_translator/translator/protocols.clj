(ns domino-translator.translator.protocols)

(defprotocol ITranslator
  (translate [this text from to])
  (detect-lang [this text])
  (get-langs [this])
  (get-translate-directions [this lang]))

(defprotocol IState
  (set-state [this new-state])
  (get-state [this]))