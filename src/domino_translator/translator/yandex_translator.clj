(ns domino-translator.translator.yandex_translator
  (:use domino-translator.translator.protocols
    [ring.util.codec :only [url-encode url-decode]])
  (:require [clj-http.client :as client]
    [clojure.data.json :as json]
    [clojure.string :as string]))

(load "yandex_secret")

(def api-url
  "https://translate.yandex.net/api/v1.5/tr.json/")

(defn map-encode [mp]
  (letfn [(inner-fn [in-map out-str]
    (if (empty? in-map)
      (str "?" (subs out-str 1))
      (let [
        [k v] (first in-map)
        new-map (dissoc in-map k)]
        (recur new-map (str out-str "&" (name k) "=" (url-encode v))))))]
  (inner-fn mp "")))

(defn api-request [suffix params]
    (-> (str api-url suffix (map-encode (merge {:key api-key} params)))
      client/get
      :body
      (#(json/read-str % :key-fn keyword))))

(defn parse-lang-list [lst]
  (letfn [(inner-fn [in-lst in-map]
    (if (empty? in-lst)
    in-map
    (let [
      [key, val] (string/split (first in-lst) #"-")
      lang-list ((keyword key) in-map)
      new-list (if (nil? lang-list)
        #{val}
        (conj lang-list val))]
      (recur (rest in-lst) (assoc in-map (keyword key) new-list)))))]
  (inner-fn lst {})))
  
;; TODO: add languages availability checks

(deftype Yandex-translator [^:volatile-mutable lang-graph]
  ITranslator

  (translate [this text from to]
    (letfn [(raw-translate [text from to]
      (->
        (api-request "translate" {:lang (str from "-" to) :text text})
        :text
        first))]
      (do
        (if (empty? lang-graph)
          (get-langs this))
        (if (contains? ((keyword from) lang-graph) to)
          (raw-translate text from to)
          (let [inter (raw-translate text from "ru")]
            (raw-translate inter "ru" to))))))

  (detect-lang [this text]
      (:lang (api-request "detect" {:text text})))

  (get-langs [this]
    (let [graph (-> (api-request "getLangs" {:ui "en"})
          :dirs
          parse-lang-list)]
      (do
        (set! lang-graph graph)
        (map name (keys lang-graph)))))

  (get-translate-directions [this lang]
    (do
      (if (empty? lang-graph)
          (get-langs this))
      ((keyword lang) lang-graph))))