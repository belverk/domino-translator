(ns domino-translator.translator.bing_translator
	(:import (java.io ByteArrayInputStream))
	(:require [clj-http.client :as client]
		[clojure.data.json :as json]
		[clojure.xml :as xml])
	(:use
    domino-translator.translator.protocols
    [ring.util.codec :only [url-encode url-decode]]))

(load "bing_secret")

(def datamarket-access-uri
  "https://datamarket.accesscontrol.windows.net/v2/OAuth2-13")

(def request
  (format "grant_type=client_credentials&client_id=%s&client_secret=%s&scope=http://api.microsofttranslator.com"
    (url-encode client-id) (url-encode client-secret)))

(defn make-request []
  (client/post
    datamarket-access-uri
    {:content-type "application/x-www-form-urlencoded"
     :body request}))

(defn get-token []
  (let [res (make-request)]
    (-> 
      (:body res)
      json/read-str
      (#(get % "access_token")))))

(defn tag-content [text]
  (let [body (:content (xml/parse (ByteArrayInputStream. (.getBytes text))))]
    (if (= (count body) 1)
      (first body)
      (map #(first (:content %)) body))))

(defn get-request [url token]
  (client/get url {:oauth-token token :throw-exceptions false}))

(defmacro req-with-check [url token]
  `(let [result# (get-request ~url ~token)]
    (cond
      (= (:status result#) 200) (-> result# :body tag-content)
      (= (:status result#) 400) (do
        (set! ~token (get-token))
        (-> (get-request ~url ~token) :body tag-content))
      :else (throw (Throwable. "Unknown error")))))

(deftype Bing-translator [^:volatile-mutable token]
  ITranslator
  IState

  (get-state [this]
    token)

  (set-state [this new-state]
    (set! token new-state))

  (translate [this text from to]
    (let [api-url "http://api.microsofttranslator.com/v2/Http.svc/Translate?text=%s&from=%s&to=%s"]
      (req-with-check (format api-url text from to) token)))

  (detect-lang [this text]
    (let [api-url "http://api.microsofttranslator.com/v2/Http.svc/Detect?text=%s"]
      (req-with-check (format api-url text) token)))

  (get-langs [this]
    (let [api-url "http://api.microsofttranslator.com/v2/Http.svc/GetLanguagesForTranslate"]
      (req-with-check api-url token)))

  (get-translate-directions [this lang]
    (get-langs this)))
