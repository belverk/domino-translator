(ns domino-translator.core)
;   (:import (java.io ByteArrayInputStream))
;   (:require [clj-http.client :as client] [clojure.data.json :as json] [clojure.xml :as xml])
;   (:use [ring.util.codec :only [url-encode url-decode]]))

; (load "secret")

; (def client-id
;   "DominoTranslator")

; (def datamarket-access-uri
;   "https://datamarket.accesscontrol.windows.net/v2/OAuth2-13")

; (def request
;   (format "grant_type=client_credentials&client_id=%s&client_secret=%s&scope=http://api.microsofttranslator.com"
;     (url-encode client-id) (url-encode client-secret)))

; (def make-request
;   (client/post 
;     datamarket-access-uri
;     {:content-type "application/x-www-form-urlencoded"
;      :body request}))

; (def get-token
;   (let [res make-request]
;     (-> 
;       (:body res)
;       json/read-str
;       (#(get % "access_token")))))

; (defn tag-content [text]
;   (-> text
;     (#(xml/parse (ByteArrayInputStream. (.getBytes %))))
;     :content
;     first))
 
; (defn detect-lang [text]
;   (let [api-url "http://api.microsofttranslator.com/v2/Http.svc/Detect?text=%s"]
;     (-> (client/get
;           (format api-url (url-encode text))
;           {:oauth-token get-token})
;       :body
;       tag-content)))
 
; (defn translate [text from to]
;   (let [api-url "http://api.microsofttranslator.com/v2/Http.svc/Translate?text=%s&from=%s&to=%s"]
;     (-> (client/get
;           (format api-url (url-encode text) (url-encode from) (url-encode to))
;           {:oauth-token get-token})
;     :body
;     tag-content)))



