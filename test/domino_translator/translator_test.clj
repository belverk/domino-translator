(ns domino-translator.translator-test
  (:use midje.sweet
    domino-translator.translator.protocols
    domino-translator.translator
    [domino-translator.translator.yandex_translator :only
      [map-encode parse-lang-list]]
    [domino-translator.translator.bing_translator :only
      [tag-content]]))

(def bing-trans
	(bing))

(def yndx-trans
  (yndx))

(def bing-avail-langs `("ar" "bg" "ca" "zh-CHS" "zh-CHT" "cs" "da" "nl" "en" "et"
  "fi" "fr" "de" "el" "ht" "he" "hi" "mww" "hu" "id" "it" "ja" "tlh" "tlh-Qaak"
  "ko" "lv" "lt" "ms" "mt" "no" "fa" "pl" "pt" "ro" "ru" "sk" "sl" "es" "sv"
  "th" "tr" "uk" "ur" "vi"))

(def yndx-directions-en
  `("ru" "hu" "uk" "de" "fr" "be" "sq" "es" "it" "da" "pt" "sk" "sl" "nl" "ca" "cs" "el" "no" "mk" "sv" "fi" "et" "lv" "lt" "tr"))

(def bing-directions-en
  `("ar" "bg" "ca" "zh-CHS" "zh-CHT" "cs" "da" "nl" "en" "et" "fi" "fr" "de" "el" "ht" "he" "hi" "mww" "hu" "id" "it" "ja" "tlh" "tlh-Qaak" "ko" "lv" "lt" "ms" "mt" "no" "fa" "pl" "pt" "ro" "ru" "sk" "sl" "es" "sv" "th" "tr" "uk" "ur" "vi"))

(def resp-body
  "<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">Одна работа без забавы - от нее тупеешь, право</string>")

(facts "Testing Bing module."
  (fact "Test languages acqusion."
    (set (get-langs bing-trans)) => 
      (set bing-avail-langs))
  (fact "Testing ru -> en translation"
    (translate bing-trans 
      "All work and no play makes Jack a dull boy" "en" "ru") =>
    "Одна работа без забавы - от нее тупеешь, право")
  (fact "Testing en -> ru translation."
    (translate bing-trans
      "Одна работа без забавы - от нее тупеешь, право" "ru" "en") =>
    "All work and no play makes Jack a dull boy")
  (fact "Testing language directions."
    (set (get-translate-directions bing-trans "en")) =>
    (set bing-directions-en)))

(facts "Testing Yandex translation module."
  (fact "Testing language detection for russian language."
    (detect-lang yndx-trans "Как хорошо, что вы пришли, Кларк.") =>
    "ru")
  (fact "Testing language detection for english language."
    (detect-lang yndx-trans "Feed your head.") =>
    "en")
  (fact "Testing ru -> en translation."
    (translate yndx-trans "The definition of insanity" "en" "ru") =>
        "Определение безумия")
  (fact "Testing en -> ru translation."
    (translate yndx-trans "The definition of insanity" "en" "ru") =>
        "Определение безумия")
  (fact "Testing unsupported fi -> es translation"
    (translate yndx-trans "suomen kieli" "fi" "es") => 
        "finlandés")
  (fact "Testing translate directions."
    (set (get-translate-directions yndx-trans "en")) =>
    (set yndx-directions-en)))

(facts "Testing auxilary translator functions"
  (fact "Testing map-encode @ yandex module."
    (map-encode {:a 1 :b "text" :c 3}) =>
    "?a=1&c=3&b=text")
  (fact "Test parsing lang list @ yandex module."
    (parse-lang-list '("en-ru" "ru-en" "en-uk" "uk-ru")) =>
      {:en #{"ru" "uk"} :ru #{"en"} :uk #{"ru"}})
  (fact "Testing tag-content @ bing module."
    (tag-content resp-body) =>
    "Одна работа без забавы - от нее тупеешь, право"))
