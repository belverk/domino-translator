(ns user
  (:require
    [clojure.tools.namespace.repl :refer (refresh refresh-all)]
    [domino-translator.translator :refer (bing yndx)])
  (:use [domino-translator.translator.protocols]))

(def system
  "Global state for debug"
  nil)

;; Utils
(defn print-tranlslate-gtaph [trans]
  (doseq [lang (get-langs trans)]
    (print 
      (str lang ": " 
      (get-translate-directions trans lang)
      "\n"))))

(defn init
  "Initialize debug system"
  []
  (do
    (alter-var-root #'system
      (constantly 
        {:bing-translator (bing)
        :yndx-translator (yndx)}))
    (def y (:yndx-translator system))
    (def b (:bing-translator system))))

(defn start
  "System start stub"
  []
  ;TODO
  )

(defn stop
  "Update state in case of system running"
  []
  (alter-var-root #'system
    (dissoc system :bing-translator :yndx-translator))
  )

(defn go
  "Initializes and starts system"
  []
  (init)
  (start)
  :ready)

(defn reset
  "Stops the system, reloads files and restart"
  []
  (stop)
  (refresh :after 'user/go))